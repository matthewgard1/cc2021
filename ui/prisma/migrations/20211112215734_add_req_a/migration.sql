-- CreateTable
CREATE TABLE `RequestA` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `requestedAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `reqUser` VARCHAR(191) NOT NULL,
    `req` VARCHAR(191) NOT NULL,
    `reqType` VARCHAR(191) NOT NULL,
    `reqMsg` VARCHAR(191) NULL,
    `reviewerUser` VARCHAR(191) NULL,
    `approvalAt` DATETIME(3) NULL,
    `rejectAt` DATETIME(3) NULL,
    `rejectMsg` VARCHAR(191) NULL,
    `res` VARCHAR(191) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
