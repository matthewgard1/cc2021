import prisma from "../../lib/prisma";
export default async function approveHandler(req, res) {
  const { method, body } = req;

  switch (method) {
    case "POST":
      try {
        let updateData = {
          reviewerUser: req.body.reviewerUser,
        };
        let time = new Date();
        time.setTime(Date.now());
        if (body.rejectMsg) {
          updateData.rejectAt = time;
          updateData.rejectMsg = body.rejectMsg;
        } else {
          updateData.approvalAt = time;
        }

        const response = await prisma.requestA.update({
          where: {
            id: Number.parseInt(body.id),
          },
          data: updateData,
        });
        res.status(200).json({
          success: true,
          id: req.body.id,
          body: response,
        });
      } catch (e) {
        console.error("Request error", e);
        res.status(500).json({ error: "Error fetching posts" });
      }
      break;
    default:
      res.setHeader("Allow", ["POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
