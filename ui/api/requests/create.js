import { runInNewContext } from "vm";
import prisma from "../../lib/prisma";
export default async function createHandler(req, res) {
  const { method, body } = req;

  switch (method) {
    case "POST":
      console.error(req.body);
      let data = {
        reqUser: body.reqUser,
        req: body.req,
        reqType: body.reqType,
      };
      if (body.reqMsg) {
        data.reqMsg = body.reqMsg;
      }
      autoComply(data);
      console.log(data);
      try {
        const response = await prisma.requestA.create({
          data,
        });

        res.status(200).json({
          success: true,
          res: response,
        });
      } catch (e) {
        console.error("Request error", e);
        res.status(500).json({ error: "Error fetching posts" });
      }
      break;
    default:
      res.setHeader("Allow", ["POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}

function autoComply(data) {
  let req = data.req;
  let time = new Date();
  time.setTime(Date.now());

  console.error("type:", data.reqType);
  console.error("req:", req);

  if (
    data.reqType === "fs" &&
    (req === "ls recordings" || req === "ls recordings -l")
  ) {
    data.approvalAt = time;
    data.reviewerUser = "white-listed";
  }
}
