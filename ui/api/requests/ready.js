import prisma from "../../lib/prisma";

export default async function readyHandler(req, res) {
  const { method, query } = req;

  switch (method) {
    case "GET":
      console.error(query);

      if (!query.type) {
        res.status(500).end(`Need Req type`);
      }

      try {
        const allRequests = await prisma.requestA.findMany({
          where: {
            approvalAt: {
              not: null,
            },
            res: null,
            reqType: query.type,
          },
          select: {
            id: true,
            req: true,
          },
        });
        console.error(allRequests);
        res.status(200).json(allRequests);
      } catch (e) {
        console.error("Request error", e);
        res.status(500).json({ error: "Error fetching posts" });
      }
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
