import prisma from "../../lib/prisma";
export default async function pendingHandler(req, res) {
  const { method, query } = req;

  switch (method) {
    case "GET":
      try {
        const allRequests = await prisma.requestA.findMany({
          where: {
            // only want where there hasn't been a review
            reviewerUser: null,
          },
          select: {
            reqUser: true,
            requestedAt: true,
            reqType: true,
            req: true,
            reqMsg: true,
          },
        });
        console.log(allRequests);
        res.status(200).json(allRequests);
      } catch (e) {
        console.error("Request error", e);
        res.status(500).json({ error: "Error fetching posts" });
      }
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
