import prisma from "../../lib/prisma";

// body takes an id and a res string
/* {
  req:{
    body:{
      id string, 
      response string
    }
  }
}
*/
export default async function respondHandler(req, res) {
  const { method } = req;

  switch (method) {
    case "POST":
      try {
        console.error(req.body);
        const response = await prisma.requestA.update({
          where: {
            id: req.body.id,
          },
          data: { res: req.body.response },
        });
        res.status(200).json({
          success: true,
          id: req.body.id,
          body: response,
        });
      } catch (e) {
        console.error("Request error", e);
        res.status(500).json({ error: "Error fetching posts" });
      }
      break;
    default:
      res.setHeader("Allow", ["POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
