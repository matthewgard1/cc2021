import React, { useState } from "react";

export default function Query(props) {
    const [text, settext] = useState("");
    const [comment, setcomment] = useState("");
   
    const handleChange = (event) => settext(event.target.value)
    const handleChangeComment = (event) => setcomment(event.target.value)
    const handleSubmit = (event) => event.preventDefault()
    
    return (
      <form onSubmit={handleSubmit}>
        <p>Command</p>
        <textarea value={text} onChange={handleChange} placeholder="Write Script Here" rows={9} cols={105} />
        <p>Comments</p>
        <textarea value={comment} onChange={handleChangeComment} placeholder="Comments" rows={6} cols={55} />
        <button type="submit" className='request_button'>Request</button>
      </form>
  );
}
