import React from "react";
import { useQuery } from "react-query";
import { FcApproval, FcDisapprove } from "react-icons/fc";
import { ImSpinner3 } from "react-icons/im";

function TermLine(p) {
  let [expanded, setExpanded] = React.useState(false);

  let resMsg = p.approvalAt
    ? `${p.res}`
    : p.rejectAt
    ? `rejected by: ${p.reviewerUser ?? ""} "${p.rejectMsg ?? ""}"`
    : "--pending--";
  return (
    <div onClick={() => setExpanded((o) => !o)}>
      {p.approvalAt ? (
        <FcApproval />
      ) : p.rejectAt ? (
        <FcDisapprove />
      ) : (
        <ImSpinner3 />
      )}
      <pre className="bash" style={expanded ? { whiteSpace: "pre-wrap" } : {}}>
        {`$${p.reqType}> ${p.req}${expanded ? "\n  " + resMsg : ""}`}
      </pre>
      <br />
    </div>
  );
}

function ReviewApp(p) {
  let username = p.username

  const { isLoading, error, data } = useQuery(
    "prevQs",
    () => fetch("/api/requests/pending").then((res) => res.json()),
    {
      refetchInterval: 3000,
    }
  );
  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;
  return (
    <>
      <div id="termlog">
        {data
          ?.filter((d) => d.reqUser === username)
          .map((d) => (
            <div>
              <TermLine {...d} />
            </div>
          ))}
      </div>
      <p>{JSON.stringify(data)}</p>
    </>
  );
}

export default ReviewApp;
