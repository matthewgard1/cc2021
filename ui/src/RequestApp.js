import React from "react";
import { useQuery, useQueryClient } from "react-query";
import { FcApproval, FcDisapprove } from "react-icons/fc";
import { ImSpinner3 } from "react-icons/im";

import { useState } from "react";
import { Menu, MenuButton, MenuList, MenuItem } from "@reach/menu-button";

function CategoryDropdown(p) {
  let [command_type, setcommand_type] = [p.get, p.set];

  return (
    <Menu>
      <MenuButton>
        {command_type} <span aria-hidden>▾</span>
      </MenuButton>
      <MenuList>
        <MenuItem onSelect={() => setcommand_type("sh")}>sh</MenuItem>
        <MenuItem onSelect={() => setcommand_type("fs")}>fs</MenuItem>
        <MenuItem onSelect={() => setcommand_type("redis")}>redis</MenuItem>
        <MenuItem onSelect={() => setcommand_type("db-france-documents")}>
          db-france-documents
        </MenuItem>
        <MenuItem onSelect={() => setcommand_type("db-us-documents")}>
          db-us-documents
        </MenuItem>
        <MenuItem onSelect={() => setcommand_type("db-china-documents")}>
          db-china-documents
        </MenuItem>
        <MenuItem onSelect={() => setcommand_type("db-users")}>
          db-users
        </MenuItem>
      </MenuList>
    </Menu>
  );
}

function TermLine(p) {
  let [expanded, setExpanded] = React.useState(false);

  let resMsg = p.approvalAt
    ? `${p.res}`
    : p.rejectAt
    ? `rejected by: ${p.reviewerUser ?? ""} "${p.rejectMsg ?? ""}"`
    : "--pending--";
  return (
    <div onClick={() => setExpanded((o) => !o)}>
      {p.approvalAt ? (
        <FcApproval />
      ) : p.rejectAt ? (
        <FcDisapprove />
      ) : (
        <ImSpinner3 />
      )}
      <pre className="bash" style={expanded ? { whiteSpace: "pre-wrap" } : {}}>
        {`$${p.reqType}> ${p.req}${expanded ? "\n  " + resMsg : ""}`}
      </pre>
      <br />
    </div>
  );
}

async function createRequest(username, req, reqType, reqMsg) {
  const rawResponse = await fetch("api/requests/create", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ reqUser: username, req, reqType, reqMsg }),
  });
  await rawResponse.json();
}

function RequestApp(p) {
  let username = p.username;
  let [command_type, setcommand_type] = useState("Command Type");
  const [query, setQuery] = useState("");
  const [comment, setcomment] = useState("");

  const queryClient = useQueryClient();

  const handleChange = (event) => setQuery(event.target.value);
  const handleChangeComment = (event) => setcomment(event.target.value);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setQuery("")
    setcomment("")
    if (query === "") return;
    if (username === "") return;
    if (command_type === "") return;
    await createRequest(username, query, command_type, comment);
    queryClient.invalidateQueries("prevQs");
  };

  const { isLoading, error, data } = useQuery(
    "prevQs",
    () => fetch("/api/requests/list").then((res) => res.json()),
    {
      refetchInterval: 3000,
    }
  );
  if (isLoading) return "Loading...";

  console.log("!!!!")
  if (error) return "An error has occurred: " + error.message;
  return (
    <>
      <div id="termlog">
        {data
          ?.filter((d) => {console.log("i"+username+JSON.stringify(d));return d.reqUser === username})
          .map((d) => (
            <div>
              <TermLine {...d} />
            </div>
          ))}
      </div>
      <CategoryDropdown get={command_type} set={setcommand_type} />
      <form onSubmit={handleSubmit}>
        <p>Command</p>
        <textarea value={query} onChange={handleChange} rows={9} cols={105} />
        <p>Comments</p>
        <textarea
          value={comment}
          onChange={handleChangeComment}
          rows={6}
          cols={55}
        />
        <button type="submit" className="request_button">
          Request
        </button>
      </form>
      <div id="termynal" data-termynal>
        <span data-ty="input">pip install spacy</span>
        <span data-ty="input" data-ty-prompt={`${command_type}$`}>
          doc = nlp(u'Hello world')
        </span>
        <span data-ty></span>
      </div>
    </>
  );
}

export default RequestApp;
