import React from "react";
import "./App.css";
import "./T.css";

import RequestApp from './RequestApp'
import ReviewApp from './ReviewApp'

function App() {
  let [app, setApp] = React.useState("request");
  let [username, setUsername] = React.useState("");

  return (
    <div className="App">
      <button
        onClick={() => {
          setApp("request");
        }}
      >
        Request
      </button>
      <button
        onClick={() => {
          setApp("approver");
        }}
      >
        Approver
      </button>
      <button
        onClick={() => {
          setApp("audit");
        }}
      >
        Audit
      </button>
      <label for="username">username: </label>
      <input
        onChange={(e) => {
          setUsername(e.target.value);
        }}
        id="username"
        name="username"
        value={username}
      />
      <h1 className="title">{app}</h1>

    {app === "request" ? <RequestApp username={username} /> : app === "approver" ? <ReviewApp username={username} /> : <div>todo</div> }  

    </div>
  );
}

export default App;
