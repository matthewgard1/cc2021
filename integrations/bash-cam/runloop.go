package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type runLoopOpts struct {
	maxLoops      int
	sleepTime     time.Duration
	commandRunner func(name string, arg ...string) CommandOutput
}

func runloop(getCommands func() ([]Command, error), send func(update) error, opts runLoopOpts) error {
	// run for max loops, or forever
	for i := 0; i < opts.maxLoops || opts.maxLoops < 0; i++ {
		commands, err := getCommands()
		if err != nil {
			return fmt.Errorf("couldn't get commands: %v", err)
		}
		for _, command := range commands {
			out, err := runCommand(command, opts.commandRunner)
			if err == nil {
				fmt.Println(out)
				err = send(update{
					Id:       command.ID(),
					Response: out,
				})
			} else {
				fmt.Println(err)
				err = send(update{
					Id:       command.ID(),
					Response: err.Error(),
				})
			}
			if err != nil {
				fmt.Printf("couldn't send response: %v\n", err)
			}
		}
		time.Sleep(opts.sleepTime)
	}
	return nil
}

func runCommand(command Command, commandRunner func(name string, arg ...string) CommandOutput) (string, error) {
	fmt.Printf("run command '%s %s' ", command.Name(), strings.Join(command.Args(), " "))
	fmt.Println(command.Args())
	cmd := commandRunner(command.Name(), command.Args()...)
	stdout, err := cmd.Output()
	if err != nil {
		return "", fmt.Errorf("err running command: %s %s %v", command.Name(), strings.Join(command.Args(), " "), err)
	}
	return string(stdout), nil
}

type command struct {
	Id  int    `json:"id"`
	Req string `json:"req"`
}

func (c *command) ID() int {
	return c.Id
}
func (c *command) Name() string {
	splitCmd := strings.Split(c.Req, " ")
	if len(splitCmd) > 0 {
		return splitCmd[0]
	}
	return ""
}
func (c *command) Args() []string {
	splitCmd := strings.Split(c.Req, " ")
	if len(splitCmd) > 1 {
		return splitCmd[1:]
	}
	return []string{}
}

func HTTPGetter(url string) ([]Command, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("couldn't GET from '%s': %v", url, err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("couldn't read res: %v", err)
	}
	fmt.Printf("get res: '%s'\n", body)
	if res.StatusCode != 200 {
		fmt.Println("body:")
		fmt.Println(string(body))
		return nil, fmt.Errorf("got non 200 status code for url: %s", url)
	}
	commands := []*command{}
	err = json.Unmarshal(body, &commands)
	if err != nil {
		return nil, fmt.Errorf("couldn't unmarshal body '%s': %v", body, err)
	}
	resCommands := []Command{}
	for _, c := range commands {
		fmt.Println("command", *c)
		resCommands = append(resCommands, c)
	}
	return resCommands, nil
}

func HTTPPoster(url string, sendbody update) error {
	body, err := json.Marshal(sendbody)
	if err != nil {
		return fmt.Errorf("couldn't read res: %v", err)
	}
	fmt.Println("body:", string(body))

	_, err = http.Post(url, "application/json", bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("couldn't POST to '%s': %v", url, err)
	}
	fmt.Println("sent req")
	return nil
}

type update struct {
	Id       int    `json:"id"`
	Response string `json:"response"`
}
