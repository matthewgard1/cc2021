package main

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/lib/pq"
)

// db-france-documents,  db-us-documents, db-china-documents, db-users

// docker run --rm -P -p 127.0.0.1:5432:5432 -e POSTGRES_PASSWORD="password" --name pg postgres:alpine
// CREATE TABLE test( id varchar primary key,username varchar(50));
// insert into test(id, username) values ('helloworld', '1234fsd');
// select * from test;
/*
// create request of type psql
curl -XPOST 'https://cc2021-eta.vercel.app/api/requests/create' -d '{"reqUser":"tomy","req":" CREATE TABLE test( id varchar primary key,username varchar(50));","reqType":"psql"}' -H "Content-Type: application/json"

// approve user command
curl -XPOST 'https://cc2021-eta.vercel.app/api/requests/approve' -d '{"reviewerUser": "sql user", "id":"23"}' -H "Content-Type: application/json"

*/

// 3 shell ones. one with aws, one for file system(file system commands are white listed), shell with curl
// redis

func getPSQLConn(connStr string) (*sql.DB, error) {
	db, err := sql.Open("postgres", connStr)
	return db, err
}

func psqlConnectionString(host string,
	port int,
	user,
	password,
	dbname string) string {
	return fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
}

func psqlWithConnString(host string, port int, user, password, dbname string) func(name string, arg ...string) CommandOutput {
	defaultConnStr := psqlConnectionString(host, port, user, password, dbname)
	return func(name string, arg ...string) CommandOutput {
		db, err := getPSQLConn(defaultConnStr)
		if err != nil {
			return &output{err: err}
		}
		defer db.Close()
		cmd := strings.Join(append([]string{name}, arg...), " ")
		rows, err := db.Query(cmd)
		if err != nil {
			return &output{err: err}
		}
		defer rows.Close()
		cols, err := rows.Columns()
		if err != nil {
			return &output{err: err}
		}

		rawResult := make([][]byte, len(cols))

		dest := make([]interface{}, len(cols))
		for i, _ := range rawResult {
			dest[i] = &rawResult[i]
		}

		fullOut := []string{}
		for _, col := range cols {
			fullOut = append(fullOut, fmt.Sprintf("%s\t", col))
		}
		fullOut = append(fullOut, "\n")

		for rows.Next() {
			err = rows.Scan(dest...)
			if err != nil {
				fmt.Println("Failed to scan row", err)
				return &output{err: err}
			}

			for _, raw := range rawResult {
				if raw == nil {
					fullOut = append(fullOut, "\\N")
				} else {
					fullOut = append(fullOut, fmt.Sprintf("%s\t", raw))
				}
			}
			fullOut = append(fullOut, "\n")
		}
		return &output{out: strings.Join(fullOut, "")}
	}
}
