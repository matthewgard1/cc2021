package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	httpEndpoint := "https://cc2021-eta.vercel.app/api"
	if len(os.Args) <= 3 {
		fmt.Println("enter max amount of loops and sleep time")
		return
	}
	maxLoops, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Printf("enter a valid number for max loops: '%s' %v\n", os.Args[1], err)
		return
	}
	sleepTime, err := strconv.Atoi(os.Args[2])
	if err != nil {
		fmt.Printf("enter a valid number for sleepTime: '%s' %v\n", os.Args[2], err)
		return
	}
	var commandRunner func(string, ...string) CommandOutput
	typeOfRunner := os.Args[3]

	var (
		host     = "localhost"
		port     = 5432
		user     = "postgres"
		password = "password"
		dbname   = "postgres"
	)
	switch typeOfRunner {
	case "bash-cam", "shell", "fs":
		// ./runner -1 20 shell 1>out-shell.txt&
		// ./runner -1 20 fs 1>out-fs.txt&
		commandRunner = bashcam
	case "psql":
		// ./runner -1 20 psql 1>out-psql.txt&
		// docker run --rm -P -p 127.0.0.1:5432:5432 -e POSTGRES_PASSWORD="password"  --name psql postgres:alpine
		commandRunner = psqlWithConnString(host, port, user, password, dbname)
	case "db-france-documents":
		// ./runner -1 20 db-france-documents 1>out-france.txt&
		// docker run --rm -P -p 127.0.0.1:5433:5432 -e POSTGRES_PASSWORD="password" --name db-france-documents postgres:alpine
		commandRunner = psqlWithConnString(host, port+1, user, password, dbname)
	case "db-us-documents":
		// ./runner -1 20 db-us-documents 1>out-us.txt&
		// docker run --rm -P -p 127.0.0.1:5434:5432 -e POSTGRES_PASSWORD="password" --name db-us-documents postgres:alpine
		commandRunner = psqlWithConnString(host, port+2, user, password, dbname)
	case "db-china-documents":
		// ./runner -1 20 db-china-documents 1>out-china.txt&
		// docker run --rm -P -p 127.0.0.1:5435:5432 -e POSTGRES_PASSWORD="password" --name db-china-documents postgres:alpine
		commandRunner = psqlWithConnString(host, port+3, user, password, dbname)
	case "db-users":
		// ./runner -1 20 db-users 1>out-users.txt&
		// docker run --rm -P -p 127.0.0.1:5436:5432 -e POSTGRES_PASSWORD="password" --name db-users postgres:alpine
		commandRunner = psqlWithConnString(host, port+4, user, password, dbname)
	case "redis":
		// ./runner -1 20 redis 1>out-redis.txt&
		// docker run --rm -P -p 127.0.0.1:6379:6379 --name redis redis:alpine
		commandRunner = redisWithConnString(host, 6379, "", 0)
	default:
		fmt.Println("arg[3] should be oneof: 'bash-cam', 'psql', 'db-france-documents', 'db-us-documents, 'db-china-documents, 'db-users', 'shell', 'fs'")
		return
	}
	getCmds := func() ([]Command, error) {
		return HTTPGetter(fmt.Sprintf("%s/requests/ready?type=%s", httpEndpoint, typeOfRunner))
	}
	sendUpdates := func(u update) error {
		return HTTPPoster(fmt.Sprintf("%s/requests/respond", httpEndpoint), u)
	}

	fmt.Printf("running loop with maxcount: %d and sleep in seconds: %d\n", maxLoops, sleepTime)
	err = runloop(getCmds, sendUpdates, runLoopOpts{maxLoops: maxLoops, sleepTime: time.Second * time.Duration(sleepTime), commandRunner: commandRunner})
	fmt.Printf("exiting with err: %v", err)
}
