module codecamp.com/bash-cam

go 1.17

require github.com/lib/pq v1.10.4

require github.com/go-redis/redis v6.15.9+incompatible // indirect
