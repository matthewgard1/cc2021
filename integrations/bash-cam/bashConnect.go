package main

import (
	"os/exec"
)

func bashcam(name string, arg ...string) CommandOutput {
	return exec.Command(name, arg...)
}

type output struct {
	out string
	err error
}

func (o *output) Output() ([]byte, error) {
	return []byte(o.out), o.err
}
