package main

type Command interface {
	ID() int
	Name() string
	Args() []string
}

type CommandGetter interface {
	Commands() ([]Command, error)
}

type ResponseSender interface {
	SendSuccess(id string, res string) error
	SendErr(id string, err error) error
}

type CommandOutput interface {
	Output() ([]byte, error)
}
