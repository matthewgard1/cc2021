package main

import (
	"fmt"

	"github.com/go-redis/redis"
)

// curl -XPOST 'https://cc2021-eta.vercel.app/api/requests/create' -d '{"reqUser":"tomy","req":"set name cameron","reqType":"redis"}' -H "Content-Type: application/json"
// 3 shell ones. one with aws, one for file system(file system commands are white listed), shell with curl
// redis

func getRedisConn(host string, port int, pass string, db int) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", host, port),
		Password: pass,
		DB:       db,
	})
	return client
}

func redisWithConnString(host string, port int, pass string, db int) func(name string, arg ...string) CommandOutput {
	return func(name string, arg ...string) CommandOutput {
		client := getRedisConn(host, port, pass, db)
		args := []interface{}{}
		for _, s := range append([]string{name}, arg...) {
			args = append(args, s)
		}
		cmdOut := client.Do(args...)
		res, err := cmdOut.Result()
		return &output{err: err, out: fmt.Sprintf("%s", res)}
	}
}
